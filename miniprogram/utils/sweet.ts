var content: Array<{ content: string }>;

content = [
    { content: "你知道我想变成什么样的人吗？变成你的人" },
    { content: "你为什么天天打我？我什么时候打你了？你明明打动了我的心" },
    { content: "和你捉迷藏我一定会输的 因为喜欢一个人是藏也藏不住的" },
    { content: "我的超能力是超喜欢你" },
    { content: "我想问你个事儿 到你心里的路该怎么走呀？" },
    { content: "我发现昨天很喜欢你 今天很喜欢你 预感明天也会喜欢你" },
    { content: "我要做一个承包商 承包你以后的幸福" },
    { content: "从今以后只能称呼你为“您” 因为你在我的心上" },
    { content: "我决定搬家了 这里没有你心里住着舒服" },
    { content: "我对你并无所企图 如果非要说我对你有企图 今生无非是图你幸福​​​​​​​​​​" },
    { content: "你会喜欢我吗？不会我教你啊" },
    { content: "我终于知道为啥单身了 为了等待现在遇见你" },
    { content: "别让我再见到你了 不然我见你一次 喜欢一次" },
    { content: "其实人间也没有什么疾苦 不过是想你罢了" },
    { content: "你是落日弥漫的橘 天边透亮的星 若有长风绕旗 那便是我在想你了" },
    { content: "如果你会游泳的话 就可以在我俩的爱河里畅游了" },
    // { content: "" },
];

export = content;