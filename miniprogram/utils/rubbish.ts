var content: Array<{ content: string }>;

content = [
    { content: "我一定能活到死" },
    { content: "能力越大，能力就越大" },
    { content: "只能赢，别不能赢" },
    { content: "但凡你有点本事，也不至于一点本事都没有" },
    { content: "听君一席话，白读十年书" },
    { content: "如果不出意外的话应该是出意外了" },
    { content: "十步杀一人，百步杀十人" },
    { content: "好是好，就是有点烂" },
    { content: "抛开内容来说，我很赞同" },
    { content: "关于明天的事我们后天就知道了" },
    { content: "他如果不是这么丑的话应该还挺帅的" },
    { content: "这个西红柿有一股番茄味" },
    { content: "我第一次去美国的时候惊呆了，我从来没在一个国家里见到那么多美国人" },
    { content: "螃蟹死之前还是活的" },
    { content: "情况嘛就是这么个情况 具体什么情况还得看情况" },
    { content: "天若有情天有情 人间正道是正道" },
    { content: "这个马铃薯怎么长得跟土豆一样" },
    // { content: "" },
];

export = content;