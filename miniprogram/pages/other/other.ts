// pages/other/other.ts
Page({
  data: {
    row: [
      [
        { name: '网抑云', image: '/images/sad.png', url: '/pages/cloud/cloud' },
        { name: '疯狂星期四', image: '/images/thurs.png', url: '/pages/thurs/thurs' },
        { name: '土味情话', image: '/images/sweet.png', url: '/pages/sweet/sweet' },
      ],
      [
        { name: '全是废话', image: '/images/rubbish.png', url: '/pages/rubbish/rubbish' },
        { name: '支付宝红包', image: '/images/alipay.png', url: '/pages/alipay/alipay' },
        { name: '每日微语', image: '/images/wei.png', url: '/pages/wei/wei' },
      ],
    ],
  },
  //options(Object)
  onShareAppMessage: function () {

  },

  onNavigateTap(e: any) {
    const { url } = e.currentTarget.dataset
    wx.navigateTo({
      url: url
    })
  },

});