// pages/rubbish/rubbish.ts
import rubbish = require("../../utils/rubbish.js");
// 在页面中定义激励视频广告
let videoAd: any

Page({
  data: {
    content: '',
    dataList: [{ content: '' }],
    max: 0
  },
  onLoad: function () {
    this.setData({
      dataList: rubbish,
      max: rubbish.length
    })
    this.nextTap()
    // 在页面onLoad回调事件中创建激励视频广告实例
    if (wx.createRewardedVideoAd) {
      videoAd = wx.createRewardedVideoAd({
        adUnitId: 'adunit-221500b8a3a3a2d6'
      })
      videoAd.onLoad(() => { })
      videoAd.onError((err: any) => { console.log(err) })
      videoAd.onClose((status: any) => {
        if (status && status.isEnded || status === undefined) {
          // 正常播放结束，下发奖励
          // continue you code
          wx.setClipboardData({
            data: this.data.content,
            success: () => {
              wx.showToast({ title: '复制成功' })
            }
          })
        } else {
          // 播放中途退出，进行提示 
          // console.log(that.data.inputVal)
        }
      })
    }
  },

  nextTap() {
    let random = Math.floor(Math.random() * this.data.max + 1) - 1;
    this.setData({
      content: this.data.dataList[random].content,
    })
  },
  onCopyTap() {
    // 用户触发广告后，显示激励视频广告
    if (videoAd) {
      videoAd.show().catch(() => {
        // 失败重试
        videoAd.load()
          .then(() => videoAd.show())
          .catch((err: any) => {
            console.log('激励视频 广告显示失败' + err)
          })
      })
    }
  },
  onShareAppMessage: function () {
    return {
      title: '听君一席话，如听一席话。',
      // path: '/pages/index/index',//这里是被分享的人点击进来之后的页面
    }
  }
})