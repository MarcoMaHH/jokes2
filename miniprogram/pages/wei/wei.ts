// pages/wei/wei.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    this.getData()
  },

  async getData() {
    const c1 = new wx.cloud.Cloud({
      resourceAppid: 'wx18e34c2164141da2',
      resourceEnv: 'prod-2gtx1lfr41c916f1',
    })
    await c1.init()
    const res = await c1.callContainer({
      "config": {
        "env": "prod-2gtx1lfr41c916f1"
      },
      "path": "/other/sentence/index",
      "header": {
        "X-WX-SERVICE": "rent5",
        "content-type": "application/json"
      },
      "method": "GET",
      "data": ""
    })
    this.setData({ list: res.data.data });
  },

  onCopyTap(e: any) {
    const { content } = e.currentTarget.dataset
    wx.setClipboardData({
      data: content,
      success: () => {
        wx.getClipboardData({
          success: function () {
            wx.showToast({
              title: '复制成功'
            })
          }
        })
      }
    })
  },
})