// pages/sweet/sweet.ts
import sweet = require("../../utils/sweet.js");
// 在页面中定义插屏广告
let interstitialAd: any

Page({
  data: {
    content: '',
    dataList: [{ content: '' }],
    max: 0
  },
  onLoad: function () {
    this.setData({
      dataList: sweet,
      max: sweet.length
    })
    this.nextTap()
    // 在页面onLoad回调事件中创建插屏广告实例
    if (wx.createInterstitialAd) {
      interstitialAd = wx.createInterstitialAd({
        adUnitId: 'adunit-ec3ace283710eabb'
      })
      interstitialAd.onLoad(() => { })
      interstitialAd.onError(() => { })
      interstitialAd.onClose(() => { })
    }
  },
  onShow: function () {
    this.showAd()
  },

  showAd: function () {
    setTimeout(function () {
      // 在适合的场景显示插屏广告
      if (interstitialAd) {
        interstitialAd.show().catch((err: any) => {
          console.error(err)
        })
      }
    }, 3000);
  },
  nextTap() {
    let random = Math.floor(Math.random() * this.data.max + 1) - 1;
    this.setData({
      content: this.data.dataList[random].content,
    })
  },
  onCopyTap() {
    wx.setClipboardData({
      data: this.data.content,
      success: () => {
        wx.showToast({ title: '复制成功' })
      }
    })
  },
  onShareAppMessage: function () {
    return {
      title: '我的超能力是超喜欢你',
      // path: '/pages/index/index',//这里是被分享的人点击进来之后的页面
    }
  }
})