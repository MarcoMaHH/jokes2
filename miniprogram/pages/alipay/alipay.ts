// pages/alipay/alipay.ts
import alipay = require("../../utils/alipay.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '772277476',
    newid: '772277476',
    list: [{ content: '' }],
    iosDialog1: false,
  },

  close() {
    this.setData({
      iosDialog1: false,
    });
  },
  onShowTap() {
    this.setData({
      iosDialog1: true,
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    this.setData({
      list: alipay,
    })
  },

  onSettingTap() {
    if (this.data.newid) {
      alipay.forEach(item => {
        item.content = item.content.replace(this.data.id, this.data.newid);
      });
      this.setData({
        id: this.data.newid,
        iosDialog1: false,
        list: alipay,
      })
    }
  },

  onInput(e: any) {
    this.setData({
      newid: e.detail.value,
    })
  },

  onCopyTap(e: any) {
    const { data } = e.currentTarget.dataset
    wx.setClipboardData({
      data,
      success: () => {
        wx.getClipboardData({
          success: function () {
            wx.showToast({
              title: '复制成功'
            })
          }
        })
      }
    })
  },
})