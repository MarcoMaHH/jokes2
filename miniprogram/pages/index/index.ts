//Page Object
Page({
  data: {
    weiyu: '世界上只有一种真正的英雄主义，那就是看清生活的真相之后，依然热爱生活。',
    imgUrl: 'https://7072-prod-5g7nwhpke8f7be0c-1305244947.tcb.qcloud.la/20221119.jpg?sign=af0bfe899f37102591ef8b69e3dfde79&t=1675960098'
  },
  //options(Object)
  onShareAppMessage: function () {

  },

  onLoad: function () {
    this.getData()
  },

  async getData() {
    const c1 = new wx.cloud.Cloud({
      resourceAppid: 'wx18e34c2164141da2',
      resourceEnv: 'prod-2gtx1lfr41c916f1',
    })
    await c1.init()
    const res = await c1.callContainer({
      "config": {
        "env": "prod-2gtx1lfr41c916f1"
      },
      "path": "/other/sentence/index",
      "header": {
        "X-WX-SERVICE": "rent5",
        "content-type": "application/json"
      },
      "method": "GET",
      "data": ""
    })
    this.setData({ "weiyu": res.data.data[0].content });
  },

});
