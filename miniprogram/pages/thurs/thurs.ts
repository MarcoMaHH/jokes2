// pages/thurs/thurs.ts
import thurs = require("../../utils/thurs.js");

Page({
  data: {
    content: '',
    dataList: [{ content: '' }],
    max: 0
  },
  onLoad: function () {
    this.setData({
      dataList: thurs,
      max: thurs.length
    })
    this.nextTap()
  },
  nextTap() {
    let random = Math.floor(Math.random() * this.data.max + 1) - 1;
    this.setData({
      content: this.data.dataList[random].content,
    })
  },
  onCopyTap() {
    wx.setClipboardData({
      data: this.data.content,
      success: () => {
        wx.showToast({ title: '复制成功' })
      }
    })
  },
  onShareAppMessage: function () {
    return {
      title: '怎么回事？疯狂星期四没人发朋友圈呢？我炸了一百多斤的鸡翅 谁来买？',
      // path: '/pages/index/index',//这里是被分享的人点击进来之后的页面
    }
  }
})