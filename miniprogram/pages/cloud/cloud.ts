// pages/cloud/cloud.ts
import util = require("../../utils/util.js");

Page({
  data: {
    content: '',
    dataList: [{ content: '' }],
    max: 0
  },
  onLoad: function () {
    this.setData({
      dataList: util,
      max: util.length
    })
    this.nextTap()
  },
  nextTap() {
    let random = Math.floor(Math.random() * this.data.max + 1) - 1;
    this.setData({
      content: this.data.dataList[random].content,
    })
  },
  onCopyTap() {
    wx.setClipboardData({
      data: this.data.content,
      success: () => {
        wx.showToast({ title: '复制成功' })
      }
    })
  },
  onShareAppMessage: function () {
    return {
      title: '改天是哪天，下次是哪次，以后是多后。',
      // path: '/pages/index/index',//这里是被分享的人点击进来之后的页面
    }
  }
})